﻿using System;

namespace isainkov03
{
    public class StudentInfo
    {
        public string NameOfGroup { get; set; }
        public char IndexOfGroup { get; set; }
        public string Specialization { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fathersname { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime DateEnter { get; set; }
        public double Performance { get; set; }

        public StudentInfo()
        {
            NameOfGroup = "CIT";
            IndexOfGroup = 'e';
            Specialization = "123 Computer engeneering";
            FirstName = "Illia";
            LastName = "Isainkov";
            Fathersname = "Vyacheslavovich";
            Birthday = new DateTime(2001, 11, 28);
            DateEnter = new DateTime(2018, 09, 01);
            Performance = 100.0;
        }

        public StudentInfo(string newNameGroup, char newGroupIndex,
            string newSpec, string newName, string newLastName, string newFName,
            DateTime newBd, DateTime newDateEnter, double newPerf)
        {
            NameOfGroup = newNameGroup;
            IndexOfGroup = newGroupIndex;
            Specialization = newSpec;
            FirstName = newName;
            LastName = newLastName;
            Fathersname = newFName;
            Birthday = newBd;
            DateEnter = newDateEnter;
            Performance = newPerf;
        }

        public override string ToString()
        {
            return $"Last name: {LastName}\nFirst name: {FirstName}\nFather`s name: {Fathersname}\n" +
                "BirthDate: " + Birthday.ToString("d") + "\n" +
                "EnterDate: " + DateEnter.ToString("d") + "\n" +
                "Group: " + NameOfGroup + "\n" +
                "Group Index: " + IndexOfGroup + "\n" +
                "Specialization: " + Specialization + "\n" +
                "Performance: " + Performance + "\n";
        }
    }
}
