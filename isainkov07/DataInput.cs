﻿using System;

namespace isainkov07
{
    public static class DataInput
    {
        public static StudentInfo InputStudent()
        {
            int day;
            int month;
            int year;

            Console.Write("Last name: ");
            string newLastName = InputName();

            Console.Write("First name: ");
            string newName = InputName();

            Console.Write("Father`s name: ");
            string newFName = InputName();

            Console.Write("Birth-day (day): ");
            day = InputInt();
            Console.Write("Birth-day (month): ");
            month = InputInt();
            Console.Write("Birth-day (year): ");
            year = InputInt();
            DateTime newBd = new DateTime(year, month, day);

            Console.Write("Date of enter (day): ");
            day = InputInt();
            Console.Write("Date of enter (month): ");
            month = InputInt();
            Console.Write("Date of enter (year): ");
            year = InputInt();
            DateTime newDateEnter = new DateTime(year, month, day);

            Console.Write("Group: ");
            string newNameGroup = InputString();

            Console.Write("Group index: ");
            char newGroupIndex = InputChar();

            Console.Write("Specialization: ");
            string newSpec = InputString();

            Console.Write("Performance: ");
            double newPerf = inputDouble();

            var tmpStudent = new StudentInfo(newNameGroup,
            newGroupIndex, newSpec, newName, newLastName, newFName, newBd,
            newDateEnter, newPerf);

            return tmpStudent;
        }

        public static string InputName()
        {
            string name = Console.ReadLine();

            while (!DataChecking.ValidateName(name))
            {
                Console.WriteLine("This info is incorrect.");
                name = Console.ReadLine();
            }

            return name;
        }

        public static int InputInt()
        {
            while (true)
            {
                try
                {
                    return Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static double inputDouble()
        {
            while (true)
            {
                try
                {
                    double value = Convert.ToDouble(Console.ReadLine());
                    return value;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static char InputChar()
        {
            while (true)
            {
                try
                {
                    return Convert.ToChar(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }


        public static string InputString()
        {
            string sentence = Console.ReadLine();

            while (!DataChecking.ValidateChar(sentence))
            {
                Console.WriteLine("This info is incorrect.");
                sentence = Console.ReadLine();
            }

            return sentence;
        }
    }
}

