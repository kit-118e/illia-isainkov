﻿using System;
using System.Collections.Generic;
using System.Text;

namespace isainkov07
{
    public class Age
    {
        public int Year { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public Age()
        {

        }
        public override string ToString()
        {
            return Year + " years " + Month + " months " + Day + " days";
        }
    }
}
