﻿using System;
using System.Collections.Generic;
using System.Text;

namespace isainkov04
{
    public class Container<T>
    {
        private T[] _items;
        public Container()
        {
            _items = new T[0];
        }

        public int Count()
        {
            return _items.Length;
        }

        public void Add(T newObj)
        {
            T[] _newItems = new T[_items.Length + 1];
            if (_items.Length > 0)
            {
                Array.Copy(_items, _newItems, _items.Length);
            }
            _newItems[_newItems.Length - 1] = newObj;
            _items = _newItems;
        }

        public void Remove(int index)
        {
            if (index < 0 || index >= _items.Length)
            {
                return;
            }
            if (_items.Length == 1)
            {
                _items = new T[0];
                return;
            }
            T[] _newItems = new T[_items.Length - 1];
            if (index > 0)
            {
                Array.ConstrainedCopy(_items, 0, _newItems, 0, index);
                if (index != _items.Length - 1)
                {
                    Array.ConstrainedCopy(_items, index + 1, _newItems, index, _items.Length - index - 1);
                }
            }
            else
            {
                Array.ConstrainedCopy(_items, 1, _newItems, 0, _items.Length - 1);
            }
            _items = _newItems;
        }
        public T this[int index]
        {
            get
            {
                return _items[index];
            }

            set
            {
                _items[index] = value;
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            T[] _newItems = new T[_items.Length];
            Array.Copy(_items, _newItems, _items.Length);
            for (int i = 0; i < _items.Length; i++)
            {
                yield return _newItems[i];
            }
        }
    }
}
