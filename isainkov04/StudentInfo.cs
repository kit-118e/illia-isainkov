﻿using System;

namespace isainkov04
{
    public class StudentInfo
    {
        public string NameOfGroup { get; set; }
        public char IndexOfGroup { get; set; }
        public string Specialization { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fathersname { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime DateEnter { get; set; }
        public double Performance { get; set; }

        public Age Age { get; }

        public StudentInfo()
        {
            NameOfGroup = "CIT";
            IndexOfGroup = 'e';
            Specialization = "123 Computer engeneering";
            FirstName = "Illia";
            LastName = "Isainkov";
            Fathersname = "Vyacheslavovich";
            Birthday = new DateTime(2001, 11, 28);
            DateEnter = new DateTime(2018, 09, 01);
            Performance = 95.0;
        }

        public StudentInfo(string newNameGroup, char newGroupIndex,
            string newSpec, string newName, string newLastName, string newFName,
            DateTime newBd, DateTime newDateEnter, double newPerf)
        {
            NameOfGroup = newNameGroup;
            IndexOfGroup = newGroupIndex;
            Specialization = newSpec;
            FirstName = newName;
            LastName = newLastName;
            Fathersname = newFName;
            Birthday = newBd;
            DateEnter = newDateEnter;
            Performance = newPerf;
            if (Birthday != null)
            {
                Age = new Age();
                var date = DateTime.Now;
                Age.Year = date.Year - Birthday.Year;
                if (date.Month >= Birthday.Month && date.Day >= Birthday.Day)
                {
                    Age.Month = date.Month - Birthday.Month;
                    Age.Day = date.Day - Birthday.Day;
                }
                else
                {
                    Age.Year -= 1;
                    Age.Month = date.Month + 11 - Birthday.Month;
                    Age.Day = DateTime.DaysInMonth(Birthday.Year, Birthday.Month) - Birthday.Day + date.Day;
                    if (Age.Day > DateTime.DaysInMonth(date.Year, date.Month))
                    {
                        Age.Month++;
                        Age.Day -= DateTime.DaysInMonth(date.Year, date.Month);
                    }
                }

            }
        }

        public override string ToString()
        {
            return $"Last name: {LastName}\nFirst name: {FirstName}\nFather`s name: {Fathersname}\n" +
                "BirthDate: " + Birthday.ToString("d") + "\n" +
                "EnterDate: " + DateEnter.ToString("d") + "\n" +
                "Group: " + NameOfGroup + "\n" +
                "Group Index: " + IndexOfGroup + "\n" +
                "Specialization: " + Specialization + "\n" +
                "Performance: " + Performance + "\n" + "Age of student: " + Age + "\n";
        }
    }
}
