﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace lab1
{
    class StudentInfo
    {
        public string Name { get; set; }
        public string DateOfBirthday { get; set; }
        public string DateOfReceipt { get; set; }
        public string GroupId { get; set; }
        public string Speciality { get; set; }
        public string Performance { get; set; }

        public void showData()
        {
            Console.WriteLine($"\n\n--- Student information ---\n");
            Console.WriteLine($"Name: ");
            Console.WriteLine(Name);
            Console.WriteLine($"Date Of Birthday: ");
            Console.WriteLine(DateOfBirthday);
            Console.WriteLine($"Date Of Receipt ");
            Console.WriteLine(DateOfReceipt);
            Console.WriteLine($"Group Id: ");
            Console.WriteLine(GroupId);
            Console.WriteLine($"Speciality: ");
            Console.WriteLine(Speciality);
            Console.WriteLine($"Performance: ");
            Console.WriteLine(Performance);
        }

    }
}
