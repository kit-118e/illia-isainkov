﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace isainkov08
{
    public class LoadStud
    {
        public static Container<StudentInfo> students = ReadFile();

        public static Container<StudentInfo> ReadFile()
        {
            Container<StudentInfo> temp = new Container<StudentInfo>();
            string LastName;
            string FirstName;
            string Fathersname;
            DateTime Birthday;
            DateTime DateEnter;
            string NameOfGroup;
            char IndexOfGroup;
            string Specialization;
            double Performance;

            string path = @"C:\Users\IlyaI\Desktop\test.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split("/");

                        LastName = infoStudent[0];
                        FirstName = infoStudent[1];
                        Fathersname = infoStudent[2];
                        Birthday = DateTime.Parse(infoStudent[3]);
                        DateEnter = DateTime.Parse(infoStudent[4]);
                        NameOfGroup = infoStudent[5];
                        IndexOfGroup = char.Parse(infoStudent[6]);
                        Specialization = infoStudent[7];
                        Performance = int.Parse(infoStudent[8]);
                        var obj = new StudentInfo(NameOfGroup, IndexOfGroup, Specialization, FirstName, LastName, Fathersname, Birthday, DateEnter, Performance);
                        temp.Add(obj);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return temp;
        }
    }
}
