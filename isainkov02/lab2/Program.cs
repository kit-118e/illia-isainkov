﻿using System;

namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new Container();
            bool key = true;
           
            while (key)
            {
                Console.WriteLine($"\nEnter or display student data? (e/d)");
                string value = Console.ReadLine();
                if (value == "e" || value == "E")
                {
                    var Student = new StudentInfo();
                    Console.WriteLine($"Name: ");
                    Student.Name = Console.ReadLine();
                    Console.WriteLine($"Date Of Birthday: ");
                    Student.DateOfBirthday = Console.ReadLine();
                    Console.WriteLine($"Date Of Receipt: ");
                    Student.DateOfReceipt = Console.ReadLine();
                    Console.WriteLine($"Group Id: ");
                    Student.GroupId = Console.ReadLine();
                    Console.WriteLine($"Speciality: ");
                    Student.Speciality = Console.ReadLine();
                    Console.WriteLine($"Performance: ");
                    Student.Performance = Console.ReadLine();
                    container.Add(Student);
                }
                else if (value == "D" || value == "d")
                {
                    key = false;
                    container.showData();
                }
                else break;
            }

            Console.WriteLine($"\nSearch element by index: ");
            var elem = int.Parse(Console.ReadLine());
            container.Serach(elem);

        }
    }
}

